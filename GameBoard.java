//Stephen McCarthy
//Assignment: HANGMAN
//DATE DUE: 10/12/2012
//Objective in this class - to declare the array. Get a random word and
//get the length of that random word.

import javabook.*;

class GameBoard {
	
	
	////////***************************DECLARE THE VARIABLES WE WILL USE*************************
	//User input variables
	private String userInput; //getting the value of the userInput
	private String theLetterIn; //used to take the first char input by user
	
	//User input variables for method to guess again when the user inputs a letter that was all ready guessed
	private String playAgain; //a string to use to convert the user's input
	private String playAgainUpper; //to convert the user's input to upper case
	private String playAgainInputUpper; //to hold the upper case input after it has been converted
	
	//variable string to convert to characters to see if letter input by user was all ready guessed
	private String againInputUpper;
	private String anotherInputUpper;
	
	//To start of our while loop
	private char theYesIn = 'Y'; //defaults to Y so we can start the game
	
	//Declare the variables to get our random word
	private int theWord;//the point in the index of the random word 
	private String wordToGuess;//the word to guess
	private int lengthOfWordToGuess;//the lenth of the word to guess
	
	
	/////////*********************************END OF VARIABLE*****************************************
	
	//Initialise an instances of our WordArray and Counters class
	WordArray anotherWordArray;
	Counters anotherCounter;
	
	
	///////// *******************CONSTRUCTOR METHOD************************************
	
	public GameBoard() {
		
		//RANDOM WORD VARIABLES
		//give the private variables default values
		this.theWord = 0;
		this.lengthOfWordToGuess = 0;
		
	}/////////*********************END CONSTRUCTOR METHOD**************************************
	
	
	
	//// METHODS OR BEHAVIOURS OF OUR GAMEBOARD OBJECT

	
	public GameBoard( OutputBox outBox, MainWindow mWindow){
	
	//instantiate our new Counter class	
	anotherCounter = new Counters();
	
	//call our getter methods in our Counter class	
	int numberOfLives = anotherCounter.getTheNumberOfLives();
	
	int badGuessesCount = anotherCounter.getTheBadGuessesCount();
	
	int correctGuessesCount = anotherCounter.getTheBadGuessesCount();
	
	
		////////***************START THE GAME LOOP LOGIC********************************
	
	//WHILE THE CHARACTER VARIABLE IS Y ENTER LOOP 
	//REMEMBER WE WANT TO STAY IN THE GAME LOOP - ONLY IF N OR O IS ENTERED WILL WE
	//EXIT IT AND THE GAME 
	
while (theYesIn == 'Y'){
	
		//call both methods to generate a random word
		generateWord();
		lengthOfTheWord();
		
		//re-assign these values again so we can enter the game loop again if the user
		//comes out of the while loop below by satisfying it's conditions. i.e.
		//if the user wins by guessing all the letters in the word or loses by guessing
		//six letters wrong.  
		
		badGuessesCount = 0;
		numberOfLives = 6;

	
///////****************************TESTING******************************************
//outBox.println("The word in your array is: " +wordToGuess);	

//print out the number of letters in the wordToGuess
//outBox.println("The length of the random word generated is: " +lengthOfWordToGuess);
////*******************************TESTING*******************************************


char[] incorrectLetters = new char[numberOfLives]; //Initialise a character array to store the correctly guessed letters

//Initialise a character array to store the correct letters guessed: maximum length can only be as long as
//the random word to guess
char[] correctLetters = new char[wordToGuess.length()];


//ADD AN UNDERSCORE TO THE PLACE OF THE LETTERS
for (int i = 0; i < correctLetters.length; i++){
correctLetters[i] = '_';
}	
	
	
	////************************************** START THE GUESSING LOGIC****************************************************************************
		
		
		//// WHILE LOOP TO START THE PLAYING OF THE GAME
		
		/// IF EITHER OF THESE CONDITIONS EVALUATE TO TRUE THEN THE USER HAS WON OR LOST
	while (!gameWon(correctLetters) && badGuessesCount <6){
		
		//*****TESTING***********
		//outBox.println("The word in your array is: " +wordToGuess);	

		//print out the number of letters in the wordToGuess
		// outBox.println("The length of the random word generated is: " +lengthOfWordToGuess);
		//*****TESTING***********
		
		
		outBox.println("Number of Lives Left: " +numberOfLives); //Print the number of lives to the screen
		
		outBox.print("Word to Guess: ");
		
		for (int i = 0; i < correctLetters.length; i++) {
			outBox.print(correctLetters[i] + " "); //Add the correct letters to the correct index position in the array
					}
        
        outBox.print("\nIncorrect Letters Guessed: " ); //Print to screen

		
		for (int i = 0; i < badGuessesCount; i++) {
			outBox.print(incorrectLetters[i] + " "); //print the incorrect letters guessed on the same line	
		}	
			
			
			
			
	
	//////****************** INPUT: SHOW THE INPUT BOX AND GET INPUT FROM THE USER*************************					
	InputBox iBox = new InputBox(mWindow);
	iBox.setIcon(iBox.NO_ICON);
	this.userInput = iBox.getString("Guess a letter or 0 to exit: "); //show the input box
	
	
	String userInputUpper; //Declare our local variable to convert userInput to Upper case

	userInputUpper = userInput.toUpperCase(); //Convert to upper case
	
	//Take the first letter input by the user, even if more than one letter entered
	//Assign it to theLetterIn of a data type character
	char theLetterIn = userInputUpper.charAt(0);
	
	
	///****************START WHILE LOOP TO ERROR CHECK FOR INVALID CHARACTER*************************************
	//If the letter input by the user is not '0' to exit or letters A - Z then it must be invalid
	//Probably an easier way to do this. Make a note to look at again
	while (theLetterIn != '0' && theLetterIn !='A' && theLetterIn !='B' && theLetterIn !='C' && theLetterIn !='D' && 
	theLetterIn !='E' && theLetterIn !='F' && theLetterIn !='G' && theLetterIn !='H' && 
	theLetterIn !='I' && theLetterIn !='J' && theLetterIn !='K' && theLetterIn !='L' && 
	theLetterIn !='M' && theLetterIn !='N' && theLetterIn !='O' && theLetterIn !='P' &&
	theLetterIn !='Q' && theLetterIn !='R' && theLetterIn !='S' && theLetterIn !='T' &&
	theLetterIn !='U' && theLetterIn !='V' && theLetterIn !='W' && theLetterIn !='X' &&
	theLetterIn !='Y' && theLetterIn !='Z'){
		
		iBox.setIcon(iBox.WARNING_ICON); //set the icon in the input box to a warning
	    this.userInput = iBox.getString("Invalid charcacter. Please guess again or 0 to exit. "); //get user input
		againInputUpper = userInput.toUpperCase() ;//Convert to upper case
		char inputValue = againInputUpper.charAt(0); //take the first character input by the user
		theLetterIn = inputValue; //assign to variable theLetterIn
		
		if (theLetterIn == '0') {
			
			System.exit(0); //Exit the game
		}
		
	  }///END WHILE LOOP FOR INVALID ERROR CHECKING
	

	////////////***********************************END ERROR CHECKING **********************************************************
	
//IF THE LETTER INPUT IS THE CHARACTER 0 THEN EXIT THE GAME	
if (theLetterIn == '0'){
	
	System.exit(0); //Exit the game
}
	else {
		
//////******ELSE DO ALL OF THIS BELOW
	
	
	////************** CHECK TO SEE IF LETTER ALL READY INPUT ******************************************************

	///////Check the incorrect letters to see if the letter guessed was all ready guessed
	for (int i = 0; i < incorrectLetters.length; i++){

	if (theLetterIn == incorrectLetters[i]) {

		this.userInput = iBox.getString("Guess again you all ready guessed the letter " +theLetterIn+ "."); //get user input
		againInputUpper = userInput.toUpperCase(); //Convert to upper case
		char inputValue = againInputUpper.charAt(0); //take the first character input by the user
		theLetterIn = inputValue; //assign to variable theLetterIn

	}//END IF 

}//END FOR LOOP TO CHECK INCORRECT LETTERS GUESSED

	//Check the correct letters to see if the letter guessed was all ready guessed
	for (int x = 0; x < correctLetters.length; x++){

	if (theLetterIn == correctLetters[x]){

		this.userInput = iBox.getString("Guess again you all ready guessed the letter " +theLetterIn+ ".");
		anotherInputUpper = userInput.toUpperCase();
		char inputValue = anotherInputUpper.charAt(0);
		theLetterIn = inputValue;

	}//END IF
	}//END FOR LOOP TO CHECK CORRECT LETTERS GUESSED
	
	///////////************************************END CHECK TO SEE IF LETTER ALL READY INPUT*********************************************************************
	
	
	
	boolean correct = false; //set our boolean to use in our if statement
		
	//loop runs equal to the length of the random word to check through the characters
	for (int i = 0; i < lengthOfWordToGuess; i++){
		if (wordToGuess.charAt(i) == theLetterIn){
			
			//check for the boolean
			correct = true;
			
			//Add the letter input by the user to the character array to store the correct letters
			
			correctLetters[i] = theLetterIn;
			//Increment the correct guesses count by 1
			correctGuessesCount++;
			
			
		}//END IF
	}//END FOR LOOP
	
	if (correct) { //IF THE LETTER INPUT IS IN THE WORD THEN DO THIS
		
		MessageBox msgBox = new MessageBox(mWindow); //Initialise and instantiate an instance of message box
		msgBox.setIcon(msgBox.NO_ICON);
		if (correctGuessesCount == 1){
			
		//Have some fun with message box here
		msgBox.show("Correct. The letter " +theLetterIn+ " \nis in the word. Continue!");
		
			}
			else if (correctGuessesCount == 2){
			msgBox.show("Correct Guess! " +theLetterIn+ " \nis in the word. Continue!");
				
			}
			else if (correctGuessesCount == 4){
			msgBox.show("Correct again. " +theLetterIn+ " is in the word. Continue!");	
			}
			else if (correctGuessesCount == wordToGuess.length()){
				msgBox.setIcon(msgBox.INFO_ICON);
				msgBox.show("Congrats. You've won. " +theLetterIn+ " is in the word.\nAll letters now guessed. The word to guess \nwas: " +wordToGuess+ ". Click OK to continue:");
			}
			else {
				msgBox.show("Correct. The letter " +theLetterIn+ " \nis in the word. You're on a roll!");
			}
		
	}
	else { //IF THE LETTER INPUT IS NOT IN THE WORD DO THIS INSTEAD
		
		MessageBox msgBox = new MessageBox(mWindow); //Initialise and instantiate an instance of message box
		msgBox.setIcon(msgBox.WARNING_ICON); //set the message box icon to a warning symbol
		incorrectLetters[badGuessesCount] = theLetterIn; //Add the letter into the inccorrect letters array
		badGuessesCount++; // Increase the number of bad guesses by one
		numberOfLives--; // Decrease the number of lives by one
		//outBox.println("Number of Wrong Guesses " +badGuessesCount); //testing
		
		if (badGuessesCount == 1){
			
		msgBox.show("The letter " +theLetterIn+ " is not in \nthe word. You will lose a life.");	
		//outBox.println("\nSorry the letter what not in the word!"); //testing
			}
			else if (badGuessesCount == 6){
				
				msgBox.show("You Lose. The word to guess was: " +wordToGuess+ " \nClick OK to continue:");
			}
			else if (badGuessesCount == 5){// give the game some personality
				msgBox.setIcon(msgBox.ERROR_ICON); //set the message box to an error symbol to warn of one life left
				msgBox.show("The letter " +theLetterIn+ " is not in \nthe word. You will lose a life. \nCareful, you only have 1 life left.");	
			}
			else {
				msgBox.show("The letter " +theLetterIn+ " is not in \nthe word. You will lose a life.");	
			}
		
	}////END ELSE	
outBox.clear(); //clear the output box each time, before continuing back to start of while loop
}
	}
		//Execute the play again method to ask the user if they want to play or exit
		//This is outside of the inside loop and is asked when the user fulfills one the
		//conditions set in the inside loop. i.e. all letters guessed or 6 wrong guesses
		playAgain(outBox, mWindow);
	}


	
}/////////////////*************************END OF WHILE LOOP*****************************************

	
	//Method to check if the game is won
	private boolean gameWon(char[] correctLetters) {
		//if there are no underscores in the 
		// correctLetters array then the game is won
	for (int i = 0; i < correctLetters.length; i++) {
		if (correctLetters[i] == '_') {
			return false;
		}
	}
	return true;
}/// *********************************END GAME WON METHOD ******************************************
	
	
	
	//////***************************START PLAY AGAIN METHOD*************************************************
	
	public void playAgain(OutputBox outBox, MainWindow mWindow){
		
		anotherCounter = new Counters(); //create an instance of our counter classe

		int badGuessesCount = anotherCounter.getTheBadGuessesCount(); //make a call to our badguesses count
			
	if (badGuessesCount == 6){
		
		InputBox inBox = new InputBox(mWindow); 
		
		this.playAgain = inBox.getString("Input Y to play again or N to exit:"); //get user input
		
		playAgainInputUpper = playAgain.toUpperCase(); // Convert user input to upper case
		char theInputValue = playAgainInputUpper.charAt(0); // Take only the first letter input if more than one

		
		if (theInputValue == 'N'){
				
			System.exit(0); // exit the game if '0' is input by the user
		}///END IF
		
		
	}///END IF
		else {
			
			InputBox inpBox = new InputBox(mWindow);

			this.playAgain = inpBox.getString("Input Y to play again or N to exit:"); //get user input
			
			playAgainInputUpper = playAgain.toUpperCase(); // Convert user input to upper case
			char theInputValue = playAgainInputUpper.charAt(0); // Take only the first letter input if more than one
			
			if (theInputValue == 'N'){

				System.exit(0); // exit the game if '0' is input by the user
			}///END IF
			
		}///END ELSE
		
}///********************************************END PLAY AGAIN METHOD*****************************************

		
	/// **************************METHOD TO GENERATE A RANDOM WORD ***************************************
	public void generateWord(){
	
		anotherWordArray = new WordArray(); //Instantiate an instance of our WordArray object
		
		//assign a random position in the index array to theWord by using the Maths class
		this.theWord = (int) (Math.random() *anotherWordArray.getTheWordArray().length);
		
	}///END GENERATE WORD METHOD
	
	///***************************** END METHOD TO GENERATE A RANDOM WORD **************************************
	
	
	//******************************METHOD TO GET THE LENGTH OF THE WORD **************************
	public void lengthOfTheWord(){	
		
			anotherWordArray = new WordArray(); //Instantiate an instance of our WordArray object
			 // get the length of the string
			
			this.wordToGuess = anotherWordArray.getTheWordArray()[theWord]; //Get the random word and store in the string variable wordToGuess

			//get the length of the word to guess and assign the integer variable to lengthOfWordToGuess
			this.lengthOfWordToGuess = wordToGuess.length();
		
	}///END LENGTH OF WORD METHOD

	//****************************** END METHOD TO GET THE LENGTH OF THE WORD **************************
	
	
	
	/////****************************************GETTER AND SETTERS******************************
	
	//int and height are parameters the callee uses to accept inputs
	///setter is to set the new field 
	public void setTheWord (int aWord){
		this.theWord = aWord;
		
	}
	// Getter method used to get the private field theWord
	public double getTheWord (){
		return(this.theWord);
		
	}
	
	//int and height are parameters the callee uses to accept inputs
	public void setTheLengthOfWordToGuess (int aLengthOfWord){
		this.lengthOfWordToGuess = aLengthOfWord;
		
	}
	public int getTheLengthOfWordToGuess (){
		return(this.lengthOfWordToGuess);
	}
	
	
}