//Stephen McCarthy
//Assignment: HANGMAN
//DATE DUE: 10/12/2012
//Objective in this class - set the default values for the counters and create getters
//and setters for them so gameboard class can use them

import javabook.*;

class Counters {
	
	private int numberOfLives = 6; //set the number of lives the user has to 6
	private int badGuessesCount = 0; //count the number of bad guesses the user inputs 
	private int correctGuessesCount = 0; //count the number of correct guesses the user inputs
	
	
	public Counters() {
		
		
		//COUNTING VARIABLES GIVE THEM DEFAULT VALUES
		this.numberOfLives = 6;
		this.badGuessesCount = 0;
		this.correctGuessesCount = 0;

		
	}////END METHOD PUBLIC COUNTERS
	
	
	
	/////GETTER AND SETTERS
	
	//int and height are parameters the callee uses to accept inputs
	///setter is to set the new field 
	public void setTheNumberOfLives (int aLife){
		this.numberOfLives = aLife;
		
	}
	// Getter method used to get the private field theWord
	public int getTheNumberOfLives (){
		return(this.numberOfLives);
		
	}
	
	//int and height are parameters the callee uses to accept inputs
	public void setTheBadGuessesCount (int aBadGuess){
		this.badGuessesCount = aBadGuess;
		
	}
	public int getTheBadGuessesCount (){
		return(this.badGuessesCount);
	}
	
	//int and height are parameters the callee uses to accept inputs
	public void setcorrectGuessesCount (int aCorrectGuess){
		this.correctGuessesCount = aCorrectGuess;
		
	}
	public int getThecorrectGuessesCount (){
		return(this.correctGuessesCount);
	}
	
	


}//// END class counters