// Stephen McCarthy
//Assignment: HANGMAN
//DATE DUE: 10/12/2012
import javabook .*;

public class App {

public static void main (String args[])	{
	App thisProgram = new App();
	   }
	public App() {
			
		//Declare objects
		MainWindow mWindow;
		OutputBox outBox;
		InputBox inBox;
		MessageBox messageBox;
		
		//Initialise an instance of our GameBoard class
		GameBoard aGameBoard;
		
		//Instantiate Objects into memory 
		mWindow = new MainWindow();
		inBox = new InputBox(mWindow);
		outBox = new OutputBox(mWindow);
		messageBox = new MessageBox(mWindow);		
		
		outBox.setTitle("Game on!"); //Set the title to our outBox
		
		//Display an opening message at the start of our game
		messageBox.setIcon(messageBox.NO_ICON); //Set NO icon in the msgBox   
		messageBox.show("Ready to Play Hangman?");
	
		outBox.setSize(420,100); //set the size of the output box
		outBox.setLocation(428,210); // set the location to just above the input box
		//Show the output box
		outBox.show();
		//Do not need to show the window
		//mWindow.show();
		//Instantiate a new instance of our GameBoard object to start things off
		aGameBoard = new GameBoard(outBox, mWindow);
	}
	}
