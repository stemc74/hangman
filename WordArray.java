//Stephen McCarthy
//Assignment: HANGMAN
//DATE DUE: 10/12/2012
//Objective in this class - to declare the array.
//get the length of that random word.


import javabook.*;



class WordArray {
	
	//Declare our word list array which we will get the random number from
	private String [] wordListArray = {"LITTER","BEANBAG","OPENING","SETTLEMENT","IPHONE","APPLE",
	"COMPUTER","PLACE","OBJECTIVE","FRAMEWORK","AUSTERITY","BUDGET","VEGTABLE","ACE","MUTTON",
	"REJECT","LEMON","HORSE","RHETORIC","FOOTBALL"};

	/////START WITH CONSTRUCTOR METHOD
	public WordArray(){
		
		
	}
	
	/////*********** GETTERS AND SETTORS
	
	
	public void setTheWordArray (String[] aWordArray){
		this.wordListArray = aWordArray;
		
	}
	
	public String[] getTheWordArray (){
		return(this.wordListArray); 
		
	}

	
}